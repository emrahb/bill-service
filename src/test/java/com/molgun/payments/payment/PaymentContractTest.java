package com.molgun.payments.payment;

import au.com.dius.pact.consumer.ConsumerPactTestMk2;
import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import com.molgun.payments.bill.domain.Payment;
import com.molgun.payments.payment.stub.ConsumerClient;
import org.apache.http.entity.ContentType;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class PaymentContractTest  extends ConsumerPactTestMk2 {
    @Override
    protected RequestResponsePact createPact(PactDslWithProvider builder) {
        Payment paymentSource = new Payment("123123", new BigDecimal(100), "1");
        Map map = new HashMap();
        map.put("Content-Type","application/json");
        return builder
                .given("test state")
                .uponReceiving("Bill consumer, take provision test")
                .headers(map)
                .path("/takeProvision")
                .method("POST")
                .body(paymentSource.toJson())
                .willRespondWith()
                .status(201)
                .toPact();
    }


    @Override
    protected String providerName() {
        return "payment_provider";
    }

    @Override
    protected String consumerName() {
        return "bill_consumer";
    }

    @Override
    protected void runTest(MockServer mockServer) throws IOException {
        Payment paymentSource = new Payment("123123", new BigDecimal(100), "1");
        Map response = new ConsumerClient(mockServer.getUrl()).post("/takeProvision", paymentSource.toJson(), ContentType.APPLICATION_JSON);
    }
}
