package com.molgun.payments.paymentorder;

import com.molgun.payments.bill.domain.*;
import com.molgun.payments.bill.domain.utils.TestUtils;
import com.molgun.payments.creditcard.domain.PaymentSource;
import com.molgun.payments.institution.domain.InstitutionCode;
import com.molgun.payments.paymentorder.application.PaymentOrderServiceImpl;
import com.molgun.payments.paymentorder.domain.PaymentOrderAlreadyExists;
import com.molgun.payments.paymentorder.infrastructure.InMemoryPaymentOrderRepository;
import org.junit.Assert;
import org.junit.Test;

public class PaymentOrderServiceTest {

    @Test(expected = PaymentOrderAlreadyExists.class)
    public void given_customer_and_subscription_that_has_payment_order_when_payment_order_is_given_then_payment_order_is_not_created() throws SubscriptionAlreadyHasPaymentOrder, InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = TestUtils.createCustomer();
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        InMemoryPaymentOrderRepository inMemoryPaymentOrderRepository = TestUtils.createPaymentOrderRepository();
        PaymentOrderServiceImpl paymentOrderService = TestUtils.createPaymentOrderService(inMemoryPaymentOrderRepository);
        paymentOrderService.giveOrder(customer, subscription, new PaymentSource("123123"));
    }
    @Test
    public void given_subscription_when_payment_order_is_given_then_payment_order_is_created() throws InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = TestUtils.createCustomer();
        Subscription subscription = TestUtils.createSubscription();
        InMemoryPaymentOrderRepository inMemoryPaymentOrderRepository = new InMemoryPaymentOrderRepository();
        PaymentOrderServiceImpl paymentService = TestUtils.createPaymentOrderService(inMemoryPaymentOrderRepository);
        paymentService.giveOrder(customer, subscription, new PaymentSource("123123"));
        Assert.assertEquals(1, inMemoryPaymentOrderRepository.count());
    }

    @Test
    public void given_wrong_customer_and_subscription_id_when_payment_order_is_given_then_payment_order_is_not_created() {
    }
}
