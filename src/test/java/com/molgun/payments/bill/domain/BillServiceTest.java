package com.molgun.payments.bill.domain;

import com.molgun.payments.bill.infrastructure.InMemoryBillRepository;
import com.molgun.payments.creditcard.domain.ProvisionException;
import org.junit.Assert;
import org.junit.Test;

import javax.naming.OperationNotSupportedException;

import static com.molgun.payments.bill.domain.utils.TestUtils.createBillRepository;
import static com.molgun.payments.bill.domain.utils.TestUtils.createBillService;

/**
 * Created by molgun on 16/05/2017.
 */
public class BillServiceTest {
    @Test
    public void given_subscription_when_bill_due_date_comes_then_bill_should_be_paid() throws InvalidCustomerId, OperationNotSupportedException, ProvisionException {
        InMemoryBillRepository inMemoryBillRepository = createBillRepository();
        BillService billService = createBillService(inMemoryBillRepository);
        billService.payDueBills();
        long unpaid = inMemoryBillRepository.countByBillStatus(BillStatus.UNPAID);
        Assert.assertEquals(1, unpaid);
    }
}
