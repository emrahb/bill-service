package com.molgun.payments.bill.domain;

import com.molgun.payments.creditcard.domain.PaymentSource;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.domain.PaymentOrderId;
import com.molgun.payments.paymentorder.domain.PaymentOrderStatus;
import org.junit.Test;

import java.math.BigDecimal;

import static com.molgun.payments.bill.domain.utils.TestUtils.*;
import static junit.framework.TestCase.assertEquals;

public class BillTest {

    @Test
    public void newly_created_bill_status_should_be_unpaid() {
        Bill bill = createBill();
        assertEquals(BillStatus.UNPAID, bill.getBillStatus());
    }

    @Test
    public void given_costumer_and_unpaid_bill_when_costumer_selects_credit_card_and_pays_unpaid_bill_then_bill_should_be_paid() throws InvalidCustomerId {
        Bill bill = createBill();
        bill.pay(new Payment("123123", BigDecimal.valueOf(123123), "123123"));
        assertEquals(BillStatus.PAID, bill.getBillStatus());
    }

    @Test
    public void given_payment_order_when_payment_order_is_canceled_then_order_status_should_be_canceled() throws InvalidCustomerId {
        PaymentOrder paymentOrder = createPaymentOrder();
        paymentOrder.cancel();
        assertEquals(PaymentOrderStatus.CANCELED, paymentOrder.getStatus());
    }

    @Test
    public void payment_order_should_be_active_when_payment_order_is_newly_created() throws InvalidCustomerId {
        PaymentOrder paymentOrder = createPaymentOrder();
        assertEquals(PaymentOrderStatus.ACTIVE, paymentOrder.getStatus());
    }

    private PaymentOrder createPaymentOrder() throws InvalidCustomerId {
        return PaymentOrder.create(PaymentOrderId.create("123123"), CustomerId.create("123123"), createSubscription(), new PaymentSource("123123"));
    }
}
