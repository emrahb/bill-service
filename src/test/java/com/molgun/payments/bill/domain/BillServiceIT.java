package com.molgun.payments.bill.domain;

import com.molgun.payments.bill.application.BillEvents;
import com.molgun.payments.bill.application.BillEventsImpl;
import com.molgun.payments.bill.infrastructure.H2BillRepository;
import com.molgun.payments.creditcard.domain.PaymentSource;
import com.molgun.payments.creditcard.domain.ProvisionException;
import com.molgun.payments.institution.domain.InstitutionCode;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.domain.PaymentOrderId;
import com.molgun.payments.paymentorder.infrastructure.H2PaymentOrderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.naming.OperationNotSupportedException;

import static com.molgun.payments.bill.domain.utils.TestUtils.*;
import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillServiceIT {

    @Autowired
    H2BillRepository billRepository;

    @Autowired
    H2PaymentOrderRepository paymentOrderRepository;

    BillEvents billEvents;

    @Before
    public void init() throws InvalidCustomerId {
        billEvents = new BillEvents() {
            @Override
            public void paid(Bill bill, Payment paymentSource) {

            }

            @Override
            public void paidBillCanceled(Bill bill) {

            }
        };
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        Subscription subscription2 = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("232324"));
        billRepository.save(new Bill(createBillId(), subscription, createAmount(), createDueDate()));
        billRepository.save(new Bill(createBillId("142142"), subscription2, createAmount(), createDueDate()));

        PaymentOrder order = PaymentOrder.create(PaymentOrderId.create("123122"), CustomerId.create("123123"),
                Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232")),
                new PaymentSource("123123"));
        paymentOrderRepository.save(order);
    }

    @Test
    public void given_subscription_when_bill_due_date_comes_then_bill_should_be_paid() throws InvalidCustomerId, OperationNotSupportedException, ProvisionException {
        BillService billService = new BillService(billRepository, paymentOrderRepository, billEvents);
        billService.payDueBills();
        long unpaid = billRepository.countByBillStatus(BillStatus.UNPAID);
        Assert.assertEquals(1, unpaid);
    }

    @Test
    public void given_costumer_and_unpaid_bill_when_costumer_selects_credit_card_and_pays_unpaid_bill_then_bill_should_be_paid() throws InvalidCustomerId, ProvisionException, OperationNotSupportedException {
        Bill bill = createBill();
        BillService billService = new BillService(billRepository, paymentOrderRepository, billEvents);
        billService.pay(bill, new Payment("123123", bill.getAmount().getAmount(), "123123"));
        assertEquals(BillStatus.PAID, bill.getBillStatus());
    }

    @Test
    public void given_costumer_and_unpaid_bill_when_costumer_selects_credit_card_that_has_no_limit_and_pays_unpaid_bill_then_bill_should_be_unpaid() throws InvalidCustomerId, OperationNotSupportedException {
//        Customer customer = createCustomer();
//        Bill bill = createBill();
//        List<CreditCard> creditCards = getCreditCards(customer);
//        BillEvents pe = new BillEventsImpl(new FeignPaymentService() {
//            @Override
//            public void takeProvision(CreditCard creditCard, Amount amount, TrackingId trackingId) {
//
//            }
//        });
//        BillService billService = new BillService(billRepository, paymentOrderRepository, pe);
//        billService.pay(bill, creditCards.get(0));
//        bill = billRepository.findOne(bill.getBillId());
//        assertEquals(BillStatus.UNPAID, bill.getBillStatus());
    }
}
