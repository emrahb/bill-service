package com.molgun.payments.bill.infrastructure;

import com.molgun.payments.bill.domain.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by molgun on 17/05/2017.
 */
public interface H2BillRepository extends CrudRepository<Bill, BillId>, BillRepository {
    public Bill save(Bill bill);

    public Bill findOne(BillId billId);

    public List<Bill> findByDueDate(DueDate dueDate);

    public long count();

    public long countByBillStatus(BillStatus status);
}
