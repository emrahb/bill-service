package com.molgun.payments.bill.application;

import com.molgun.payments.bill.domain.BillId;
import com.molgun.payments.bill.domain.Payment;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;

/**
 * Created by molgun on 25/05/2017.
 */
public class PaymentDTO {

    private String billId;
    private String cardNumber;
    private BigDecimal amount;
    private String trackingId;

    public PaymentDTO() {
    }

    public PaymentDTO(String billId, String cardNumber, BigDecimal amount, String trackingId) {
        this.billId = billId;
        this.cardNumber = cardNumber;
        this.amount = amount;
        this.trackingId = trackingId;
    }

    public BillId asBillId() {
        return new BillId(billId);
    }

    public Payment asPayment() {
        return new Payment(cardNumber, amount, trackingId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaymentDTO that = (PaymentDTO) o;

        return new EqualsBuilder()
                .append(billId, that.billId)
                .append(cardNumber, that.cardNumber)
                .append(amount, that.amount)
                .append(trackingId, that.trackingId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(billId)
                .append(cardNumber)
                .append(amount)
                .append(trackingId)
                .toHashCode();
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    public String getBillId() {
        return billId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getTrackingId() {
        return trackingId;
    }
}
