package com.molgun.payments.bill.application;

import com.molgun.payments.bill.domain.Bill;
import com.molgun.payments.bill.domain.BillId;
import com.molgun.payments.bill.domain.BillService;
import com.molgun.payments.bill.domain.Payment;
import com.molgun.payments.bill.infrastructure.H2BillRepository;
import com.molgun.payments.creditcard.domain.PaymentService;
import com.molgun.payments.paymentorder.domain.PaymentOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class BillController {

    @Autowired
    H2BillRepository billRepository;

    BillService billService;

    @Autowired
    PaymentOrderRepository paymentOrderRepository;

    @Autowired
    PaymentService paymentService;

    BillEvents billEvents;

    @PostConstruct
    public void init() {
        this.billEvents = new BillEventsImpl(paymentService);
        this.billService = new BillService(billRepository, paymentOrderRepository, billEvents);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/bill/pay")
    public void pay(@RequestBody PaymentDTO dto) {
        BillId billId = dto.asBillId();
        Payment payment = dto.asPayment();
        Bill bill = billRepository.findOne(billId);
        billService.pay(bill, payment);
    }
}
