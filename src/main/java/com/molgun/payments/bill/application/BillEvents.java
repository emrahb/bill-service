package com.molgun.payments.bill.application;

import com.molgun.payments.bill.domain.Bill;
import com.molgun.payments.bill.domain.Payment;

/**
 * Created by molgun on 16/05/2017.
 */
public interface BillEvents {

    public void paid(Bill bill, Payment paymentSource);

    public void paidBillCanceled(Bill bill);
}
