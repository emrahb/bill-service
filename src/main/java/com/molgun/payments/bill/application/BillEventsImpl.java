package com.molgun.payments.bill.application;

import com.molgun.payments.bill.domain.Bill;
import com.molgun.payments.creditcard.domain.PaymentService;
import com.molgun.payments.bill.domain.Payment;

public class BillEventsImpl implements BillEvents {

    private PaymentService paymentService;

    public BillEventsImpl(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Override
    public void paid(Bill bill, Payment paymentSource) {
        paymentService.takeProvision(paymentSource);
    }

    @Override
    public void paidBillCanceled(Bill bill) {

    }
}
