package com.molgun.payments.bill.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DueDate {

    private Date date;

    public DueDate() {
    }

    public DueDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        this.date = calendar.getTime();
    }

    public Date getDate() {
        return date;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(dateToBillDate(date))
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final DueDate dueDate = (DueDate) o;

        return sameValueAs(dueDate);
    }

    public boolean sameValueAs(DueDate dueDate) {
        return dueDate != null && new EqualsBuilder()
                .append(dateToBillDate(date), dateToBillDate(dueDate.getDate()))
                .isEquals();
    }

    private String dateToBillDate(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = df.format(date);
        return dateString;
    }
}
