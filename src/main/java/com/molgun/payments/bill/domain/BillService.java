package com.molgun.payments.bill.domain;

import com.molgun.payments.bill.application.BillEvents;
import com.molgun.payments.creditcard.domain.ProvisionException;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.domain.PaymentOrderRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.OperationNotSupportedException;
import java.util.Date;
import java.util.List;

public class BillService {

    private BillRepository billRepository;
    private PaymentOrderRepository paymentOrderRepository;
    private BillEvents billEvents;

    public BillService(BillRepository billRepository,
                       PaymentOrderRepository paymentOrderRepository,
                       BillEvents billEvents) {
        this.billRepository = billRepository;
        this.paymentOrderRepository = paymentOrderRepository;
        this.billEvents = billEvents;
    }

    @Transactional
    public void pay(Bill bill, Payment paymentSource) {
        bill.pay(paymentSource);
        billRepository.save(bill);
        billEvents.paid(bill, paymentSource);
    }

    public void payDueBills() throws ProvisionException, OperationNotSupportedException {
        List<Bill> bills = billRepository.findByDueDate(new DueDate(new Date()));
        for (Bill bill : bills) {
            PaymentOrder paymentOrder = paymentOrderRepository.findBySubscription(bill.getSubscription());
            if (paymentOrder != null) {
                Payment payment = new Payment(
                        paymentOrder.getPaymentSource().getCardNumber(),
                        bill.getAmount().getAmount(),
                        "123123");
                pay(bill, payment);
            }
        }
    }
}
