package com.molgun.payments.creditcard.domain;

import com.molgun.payments.bill.domain.Payment;

/**
 * Created by molgun on 25/05/2017.
 */
public interface PaymentService {

    public void takeProvision(Payment paymentSource);
}
