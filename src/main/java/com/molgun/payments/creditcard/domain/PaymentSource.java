package com.molgun.payments.creditcard.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by molgun on 25/05/2017.
 */
public class PaymentSource {
    private String cardNumber;

    public PaymentSource() {
    }

    public PaymentSource(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaymentSource that = (PaymentSource) o;

        return new EqualsBuilder()
                .append(cardNumber, that.cardNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(cardNumber)
                .toHashCode();
    }
}
