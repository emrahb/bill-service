package com.molgun.payments.creditcard.domain;


import com.molgun.payments.bill.domain.Payment;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by molgun on 18/05/2017.
 */
@FeignClient("payment-service")
public interface FeignPaymentService extends PaymentService {
    @RequestMapping(method = RequestMethod.POST, value = "/takeProvision", consumes = "application/json")
    public void takeProvision(@RequestBody Payment paymentSource);
}
