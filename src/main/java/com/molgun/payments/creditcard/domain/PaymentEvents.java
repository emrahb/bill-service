package com.molgun.payments.creditcard.domain;

public interface PaymentEvents {
    public void couldNotProvisioned(TrackingId trackingId);
}
