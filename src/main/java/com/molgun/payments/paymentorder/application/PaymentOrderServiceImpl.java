package com.molgun.payments.paymentorder.application;

import com.molgun.payments.bill.domain.Customer;
import com.molgun.payments.bill.domain.Subscription;
import com.molgun.payments.creditcard.domain.PaymentSource;
import com.molgun.payments.paymentorder.domain.*;

import javax.transaction.Transactional;
import java.util.UUID;


public class PaymentOrderServiceImpl implements PaymentOrderService {

    private final PaymentOrderRepository repository;

    public PaymentOrderServiceImpl(PaymentOrderRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void giveOrder(Customer customer, Subscription subscription, PaymentSource paymentSource) throws PaymentOrderAlreadyExists {
        PaymentOrder paymentOrder = repository.findBySubscription(subscription);
        if (paymentOrder != null) {
            throw new PaymentOrderAlreadyExists();
        }

        PaymentOrder newOrder = PaymentOrder.create(PaymentOrderId.create(UUID.randomUUID().toString()), customer.getCustomerId(), subscription, paymentSource);
        repository.save(newOrder);
    }
}
