package com.molgun.payments.paymentorder.application;

import com.molgun.payments.bill.domain.*;
import com.molgun.payments.creditcard.domain.PaymentSource;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.institution.domain.InstitutionCode;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class PaymentOrderRequestDTO {

    private String customerId;
    private String institutionCode;
    private String subscriptionId;
    private String cardNumber;

    public PaymentOrderRequestDTO() {
    }

    public PaymentOrderRequestDTO fromPaymentOrder(PaymentOrder paymentOrder) {
        customerId = paymentOrder.getCustomerId().getId();
        institutionCode = paymentOrder.getSubscription().getInstitutionCode().getInstitutionCode();
        subscriptionId = paymentOrder.getSubscription().getSubscriptionId().getId();
        cardNumber = paymentOrder.getPaymentSource().getCardNumber();
        return this;
    }

    public Subscription asSubscription() {
        InstitutionCode ic = InstitutionCode.create(getInstitutionCode());
        SubscriptionId si = SubscriptionId.create(getSubscriptionId());
        return Subscription.create(ic, si);
    }

    public Customer asCustomer() throws InvalidCustomerId {
        return Customer.create(CustomerId.create(getCustomerId()));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaymentOrderRequestDTO that = (PaymentOrderRequestDTO) o;

        return new EqualsBuilder()
                .append(customerId, that.customerId)
                .append(institutionCode, that.institutionCode)
                .append(subscriptionId, that.subscriptionId)
                .append(cardNumber, that.cardNumber)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(customerId)
                .append(institutionCode)
                .append(subscriptionId)
                .append(cardNumber)
                .toHashCode();
    }

    public PaymentSource asPaymentSource() {
        return new PaymentSource(getCardNumber());
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
