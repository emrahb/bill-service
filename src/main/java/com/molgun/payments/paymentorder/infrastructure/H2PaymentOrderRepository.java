package com.molgun.payments.paymentorder.infrastructure;

import com.molgun.payments.creditcard.domain.PaymentSource;
import com.molgun.payments.paymentorder.domain.PaymentOrderRepository;
import com.molgun.payments.bill.domain.Subscription;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.domain.PaymentOrderId;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by molgun on 17/05/2017.
 */
public interface H2PaymentOrderRepository extends CrudRepository<PaymentOrder, PaymentOrderId>, PaymentOrderRepository {

    public PaymentOrder save(PaymentOrder paymentOrder);

    public PaymentOrder findOne(PaymentOrderId paymentOrderId);

    public PaymentOrder findBySubscription(Subscription subscription);

    public long count();

    public List<PaymentOrder> findByPaymentSource(PaymentSource paymentSource);
}
