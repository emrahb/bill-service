package com.molgun.payments.paymentorder.infrastructure;

import com.molgun.payments.bill.domain.Subscription;
import com.molgun.payments.creditcard.domain.PaymentSource;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.domain.PaymentOrderId;
import com.molgun.payments.paymentorder.domain.PaymentOrderRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemoryPaymentOrderRepository implements PaymentOrderRepository {

    private Map<PaymentOrderId, PaymentOrder> paymentOrders;

    public InMemoryPaymentOrderRepository() {
        this.paymentOrders = new HashMap<>();
    }

    public PaymentOrder save(PaymentOrder paymentOrder) {
        paymentOrders.put(paymentOrder.getPaymentOrderId(), paymentOrder);
        return paymentOrder;
    }

    public PaymentOrder findOne(PaymentOrderId paymentOrderId) {
        return paymentOrders.get(paymentOrderId);
    }

    public List<PaymentOrder> findByPaymentSource(PaymentSource creditCard) {
        List<PaymentOrder> orders =
                paymentOrders
                        .values()
                        .stream()
                        .filter((o) -> o.getPaymentSource().equals(creditCard))
                        .collect(Collectors.toList());
        if (orders.isEmpty()) {
            return null;
        }
        return orders;
    }

    public PaymentOrder findBySubscription(Subscription subscription) {
        List<PaymentOrder> orders =
                paymentOrders
                        .values()
                        .stream()
                        .filter((o) -> o.getSubscription().equals(subscription))
                        .collect(Collectors.toList());
        if (orders.isEmpty()) {
            return null;
        }
        return orders.get(0);
    }

    public long count() {
        return paymentOrders.size();
    }
}
