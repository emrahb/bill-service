package com.molgun.payments.paymentorder.domain;

import com.molgun.payments.bill.domain.CustomerId;
import com.molgun.payments.bill.domain.Subscription;
import com.molgun.payments.creditcard.domain.PaymentSource;
import org.apache.commons.lang3.Validate;


/**
 * Created by molgun on 16/05/2017.
 */

public class PaymentOrder {

    private PaymentOrderId paymentOrderId;
    private Subscription subscription;
    private PaymentOrderStatus status;
    private PaymentSource paymentSource;
    private CustomerId customerId;

    private PaymentOrder(PaymentOrderId paymentOrderId, CustomerId customerId, Subscription subscription, PaymentSource paymentSource) {
        this.paymentOrderId = paymentOrderId;
        this.subscription = subscription;
        this.customerId = customerId;
        this.paymentSource = paymentSource;
        this.status = PaymentOrderStatus.ACTIVE;
    }

    public PaymentOrder() {
    }

    public static PaymentOrder create(PaymentOrderId paymentOrderId,CustomerId customerId, Subscription subscription, PaymentSource creditCard) {
        Validate.notNull(subscription);
        Validate.notNull(customerId);
        Validate.notNull(creditCard);
        return new PaymentOrder(paymentOrderId, customerId, subscription, creditCard);
    }

    public PaymentOrderId getPaymentOrderId() {
        return paymentOrderId;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public PaymentOrderStatus getStatus() {
        return status;
    }

    public void cancel() {
        this.status = PaymentOrderStatus.CANCELED;
    }

    public PaymentSource getPaymentSource() {
        return paymentSource;
    }

    public CustomerId getCustomerId() {
        return customerId;
    }
}
