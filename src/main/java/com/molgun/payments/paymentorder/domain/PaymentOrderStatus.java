package com.molgun.payments.paymentorder.domain;

/**
 * Created by molgun on 16/05/2017.
 */
public enum PaymentOrderStatus {
    ACTIVE("ACTIVE"), CANCELED("CANCELED");

    private String value;

    PaymentOrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
