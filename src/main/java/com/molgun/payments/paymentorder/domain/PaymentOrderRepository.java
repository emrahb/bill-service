package com.molgun.payments.paymentorder.domain;

import com.molgun.payments.bill.domain.Subscription;
import com.molgun.payments.creditcard.domain.PaymentSource;

import java.util.List;

/**
 * Created by molgun on 17/05/2017.
 */
public interface PaymentOrderRepository {
    public PaymentOrder save(PaymentOrder paymentOrder);

    public PaymentOrder findOne(PaymentOrderId paymentOrderId);

    public PaymentOrder findBySubscription(Subscription subscription);

    public long count();

    public List<PaymentOrder> findByPaymentSource(PaymentSource paymentSource);
}
